from django.conf.urls import url
from django.urls import path, include
from .views import *
from .models import *
from rest_framework import routers, serializers, viewsets


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'flats', FlatViewSet)

urlpatterns =[
    path('', index_response, name='index'),
    path('start', startpage_response, name='start'),
    path('list', flatlist_response, name='flat_list'),
    path('flatadd', flatadd_response, name='flat_add'),
    path('list/<int:id>', flatdetails_response, name='flat_details'),
    path('about-us', aboutus_response, name='aboutus'),
    path('contact', contact_response, name='contact'),
    url(r'', include(router.urls)),
    url(r'apo', include('rest_framework.urls', namespace='rest_framework'))
]
