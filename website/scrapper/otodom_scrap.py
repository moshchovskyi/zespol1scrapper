import time
import random

import requests
from bs4 import BeautifulSoup
import re
import csv
import time

# class Flat(models.Model):
#     title = models.CharField(max_length=200)
#     code = models.CharField(max_length=20)
#     url = models.TextField(max_length=300)
#     price = models.DecimalField(max_digits=12, decimal_places=2, null=True)
#     price_m2 = models.DecimalField(max_digits=10, decimal_places=2, null=True)
#     rooms = models.PositiveSmallIntegerField(null=True)
#     area = models.DecimalField(max_digits=10, decimal_places=2, null=True)
#     city = models.CharField(max_length=20, null=True)
#     district = models.CharField(max_length=50, null=True)
#     foto = models.ImageField(null=True, blank=True)
#     description = models.TextField(default="")

offers_per_site = 72
url = "https://www.otodom.pl/sprzedaz/mieszkanie/warszawa/?search%5Bregion_id%5D=7&search%5Bsubregion_id%5D=197&search%5Bcity_id%5D=26&nrAdsPerPage=" + str(offers_per_site)
print(url)
mainContent = requests.get(url)

# Printing Response
print(mainContent)

# Parsing html
soup = BeautifulSoup(mainContent.text, "lxml")

title_list =[]
code_list=[]
url_list =[]
rooms_list=[]
prices_list=[]
pricesm2_list=[]
areas_list=[]
cities_list = []
districts_list =[]
photo_list = []

data_row = []


# getting total number of offers
articles_total = soup.find('div', class_='offers-index').get_text().split('\n')[3]
# extracting digits only ant converting to INT
total_number = int(''.join(x for x in articles_total if x.isdigit()))
print(f'Liczba ofert = {total_number}')

total_pages = total_number//offers_per_site # dividing wihout remainder
# Adding one site for remainder
if total_number%offers_per_site!=0:
    total_pages += 1

print(f'Liczba stron z ofertami = {total_pages} przy {offers_per_site} ofertach na stronę.')

current_page = 1

while True:
    n = 0
    data_dict = {}
    articles = soup.find_all('article')
    for item in articles:

        # Finding titles
        titles = item.find_all(class_='offer-item-title')
        for title in titles:
            individual_title = title.get_text()
            title_list.append(individual_title)

        #Finding codes:
        individual_code = item.get_attribute_list('data-item-id')[0]
        code_list.append(individual_code)

        # Finding offers urls:
        individual_url = item.get_attribute_list('data-url')[0]
        url_list.append(individual_url)


        # Finding number of rooms

        # for item in articles:
        room_ul_all = item.find_all("ul")
        for ul in room_ul_all:
            rooms = ul.find_all('li', class_='offer-item-rooms')
            for room in rooms:
                individual_rooms = room.get_text()
                # Extracting numbers from the string
                rooms_number = int(re.search(r'\d+', individual_rooms).group())
                rooms_list.append(rooms_number)


        # Finding prices and savings as ints

        # for item in articles:
        price_ul_all = item.find_all("ul")
        for ul in price_ul_all:
            prices = ul.find_all('li', class_='offer-item-price')
            for price in prices:
                individual_price = price.get_text()
                # Extracting numbers from the string
                try:
                    price_int = int(''.join(x for x in individual_price if x.isdigit()))
                except:
                    print_int = None
                prices_list.append(price_int)


        # Finding price/m2 as saving as ints

        # for item in articles:
        pricem2_ul_all = item.find_all("ul")
        for ul in pricem2_ul_all:
            pricesm2 = ul.find_all('li', class_='offer-item-price-per-m')
            for pricem2 in pricesm2:
                individual_pricem2 = pricem2.get_text()
                # Extracting numbers from the string
                individual_pricem2_digits = ''.join(e for e in individual_pricem2 if e.isdigit())
                # regex for all digits not following letters (to excl. 2 originated from m2)
                pricem2_digits = int(re.search('(?<![A-Za-z0-9.])[0-9.]+', individual_pricem2_digits).group())
                pricesm2_list.append(pricem2_digits)


        # Finding area (m2) and saving as floats

        area_ul_all = item.find_all("ul")
        for ul in area_ul_all:
            areas = ul.find_all('li', class_='offer-item-area')
            for area in areas:
                individual_area = area.get_text()
                # Extracting numbers from the string dd.dd followed by ' m2'
                replaced_comma = individual_area.replace(',','.')
                area_digits_compiled = re.compile('\d*\.?\d+')
                area_float = [float(i) for i in area_digits_compiled.findall(replaced_comma)][0]
                areas_list.append(area_float)


        # Finding cities and districts

        individual_localisation = item.find('p').contents[1]
        individual_city = individual_localisation.split(',')[0]
        individual_district = individual_localisation.split(',')[1]
        cities_list.append(individual_city)
        districts_list.append(individual_district)


        # Finding photo url
        individual_photo = item.find(class_='img-cover lazy').get_attribute_list('data-src')[0]
        photo_list.append(individual_photo)


    print(f'ended round {current_page}')

    current_page += 1
    if current_page > 3: #total_pages:
        print(f'Liczba rekordów: {len(data_row)}')
        break

    time.sleep(random.randint(10, 15)/10.0)
    url = "https://www.otodom.pl/sprzedaz/mieszkanie/warszawa/?search%5Bregion_id%5D=7&search%5Bsubregion_id%5D=197&search%5Bcity_id%5D=26&nrAdsPerPage=" + str(offers_per_site) + "&page=" + str(current_page)
    mainContent = requests.get(url)
    print(url)
    print(mainContent)
    soup = BeautifulSoup(mainContent.text, "lxml")


n = 0
data_dict = {}
for elem in code_list:
    try:
        data_dict = {'title': title_list[n], 'code': code_list[n], 'url': url_list[n], 'rooms': rooms_list[n],
                     'price': prices_list[n], 'pricem2': pricesm2_list[n], 'area': areas_list[n],
                     'city': cities_list[n], 'district': districts_list[n], 'foto': photo_list[n]}

    except:
        print(f'Error while compiling data_dict with article {n}')
    data_row.append(data_dict)
    n += 1

def data_array_of_dicts():
    all_data = data_row
    return all_data




# with open('otodom2.csv', 'w', encoding='utf8', newline='') as f:
#     fc = csv.DictWriter(f,
#                         fieldnames=data_row[0].keys(),
#
#                        )
#     fc.writeheader()
#     fc.writerows(data_row)

print(f'Process Ended')