from django.db import models

# Create your models here.
class Flat(models.Model):
    title = models.CharField(max_length=200)
    code = models.CharField(max_length=20)
    url = models.TextField(max_length=300)
    price = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    price_m2 = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    rooms = models.PositiveSmallIntegerField(null=True)
    area = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    city = models.CharField(max_length=20, null=True)
    district = models.CharField(max_length=50, null=True)
    foto = models.URLField(null=True, blank=True)
    description = models.TextField(default="")

    def __str__(self):
        return f"{self.title} - {self.price} - {self.city}"

    class Meta:
        verbose_name = "Mieszkanie"
        verbose_name_plural = "Mieszkania"

