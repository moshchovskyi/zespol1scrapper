from .models import Flat
from .otodom_scrap import data_array_of_dicts



def selection_of_records():

    n_of_records = 0
    n = 0
    list_of_codes = []
    n_of_copies = 0
    data_row = data_array_of_dicts()

    for elem in data_row:

        if elem['code'] not in list_of_codes:
            n += 1
            try:
                offer = Flat(
                    title=elem['title'],
                    code=elem['code'],
                    url=elem['url'],
                    price=elem['price'],
                    price_m2=elem['pricem2'],
                    rooms=elem['rooms'],
                    area=elem['area'],
                    city=elem['city'],
                    district=elem['district'],
                    foto=elem['foto']
                )
                offer.save()
                n_of_records += 1
            except:
                print(f'Record {n} input failed')

        else:
            n_of_copies += 1
            n += 1
        list_of_codes.append(elem['code'])

    return f' Liczba rekordów dodanych: {n_of_records}, \n Liczba obrotów: {n}, \n Liczba kodów unikalnych: {len(list_of_codes)}, Liczba kopii: {n_of_copies}, Liczba rekordów źródła: {len(list_of_codes)+n_of_copies} .'





