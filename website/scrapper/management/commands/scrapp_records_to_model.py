from django.core.management.base import BaseCommand, CommandError
from scrapper.save_records_to_model import selection_of_records #Warning - PyCharm suggest possible error which does not exist

class Command(BaseCommand):
    def handle(self, *args, **options):
        selected_record = selection_of_records()

        self.stdout.write(self.style.SUCCESS(f'{selected_record}'))

