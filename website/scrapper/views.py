from django.shortcuts import render, redirect, get_object_or_404
from .models import *
from .forms import *
from rest_framework import viewsets, serializers
from rest_framework import permissions



# Create your views here.
def startpage_response(request):
    result = None
    if request.method == "POST":
        form_obj = FindFlatForm(request.POST)
        if form_obj.is_valid():
            data = form_obj.cleaned_data
            data['price_min'] = data['price_min'] or 0.0
            data['price_max'] = data['price_max'] or 0.0
            data['area_min'] = data['area_min'] or 0.0
            data['area_max'] = data['area_max'] or 0.0
            data['district'] = data['district'] or ''
            data['title'] = data['title'] or ''

            result = Flat.objects.all()
            if data['price_min']>0:
                result = result.filter(price__gte=data['price_min'])
            if data['price_max']>0:
                result = result.filter(price__lte=data['price_max'])
            if data['area_min']>0:
                result = result.filter(area__gte=data['area_min'])
            if data['area_max']>0:
                result = result.filter(area__lte=data['area_max'])
            if len(data['district']):
                result = result.filter(district=data['district'])
            result = result.order_by('-price')
    else:
        form_obj = FindFlatForm()

    return render(request, 'start.html',
                  {'form': form_obj, 'result': result})


def flatlist_response(request):
    all_flat = Flat.objects.all()
    return render(request, 'flat-list.html', {'flats' : all_flat})


def flatadd_response(request):
    form = FlatForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect(flatlist_response)
    return render(request, "flat-add.html", {"form":form})


def flatdetails_response(request, id):
    flat_obj = get_object_or_404(Flat, pk=id)
    return render(request, "flat-details.html", {'flat': flat_obj})


def index_response(request):
    return render(request, "index.html")


def aboutus_response(request):
    return render(request, "aboutus.html")


def contact_response(request):
    return render(request, "contact.html")

# Serializers define the API representation.
class FlatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flat
        fields = ['title', 'code']
        permission_classes = [permissions.IsAuthenticated]

# ViewSets define the view behavior.
class FlatViewSet(viewsets.ModelViewSet):
    queryset = Flat.objects.all()
    serializer_class = FlatSerializer
    permission_classes = [permissions.IsAuthenticated]
