# Generated by Django 3.0.5 on 2020-04-28 09:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Flat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('code', models.CharField(max_length=20)),
                ('url', models.TextField(max_length=300)),
                ('price', models.DecimalField(decimal_places=2, max_digits=12, null=True)),
                ('price_m2', models.DecimalField(decimal_places=2, max_digits=10, null=True)),
                ('rooms', models.PositiveSmallIntegerField(null=True)),
                ('area', models.DecimalField(decimal_places=2, max_digits=10, null=True)),
                ('city', models.CharField(max_length=20, null=True)),
                ('district', models.CharField(max_length=50, null=True)),
                ('foto', models.ImageField(blank=True, null=True, upload_to='')),
                ('description', models.TextField(default='')),
            ],
            options={
                'verbose_name': 'Mieszkanie',
                'verbose_name_plural': 'Mieszkania',
            },
        ),
    ]
