from django import forms
from .models import *

class FlatForm(forms.ModelForm):

    class Meta:
        model = Flat
        fields = "__all__"


class FindFlatForm(forms.Form):
    title = forms.CharField(max_length=50, required=False)

    price_min = forms.IntegerField(required=False, help_text="Wpisz cenę minimalną")
    price_max = forms.IntegerField(required=False, help_text='Wpisz cenę maksymalną')

    area_min = forms.FloatField(required=False, help_text="Wpisz powierzchnię minimalną")
    area_max = forms.FloatField(required=False, help_text="Wpisz powierzchnię maksymalną")

    district = forms.ChoiceField(required=False, choices=[])

    def __init__(self, *args, **kwargs):
        super(FindFlatForm, self).__init__(*args, **kwargs)
        self.fields['title'].label = "Wyszukaj mieszkanie, wpisz frazę"
        self.fields['district'].choices = [('', '-----------')] + list(Flat.objects.all().order_by('district').values_list("district", "district").distinct())


    def clean(self):
        clean_data = super(FindFlatForm, self).clean()
        price_min = clean_data.get('price_min')
        price_max = clean_data.get('price_max')
        area_min = clean_data.get('area_min')
        area_max = clean_data.get('area_max')
        district = clean_data.get('district')
        title = clean_data.get('title')

        if not title and not price_min and not price_max \
            and not area_max and not area_min \
            and not district:
            raise forms.ValidationError("Podaj przynajmniej jeden warunek")

        if (price_min and price_min<0) or (price_max and price_max<0):
            raise forms.ValidationError("Niepoprawne wartości cen")
